The app showcases the use of newsapi in a Android environment
https://newsapi.org/ 

News are showed according to saved keywords in configuration tab. These news can be saved and read later.




![](images/actualités.png)  ![](images/mots_clés.png)  ![](images/archives.png)  ![](images/détails.png)




The app implements MVVM architecture using Android/ Kotlin, liveData, kotlin Flow, viewbinding, retrofit 2, Room, coroutines, newsapi, dagger hilt

It contains following packages:
1. data: It contains all the data accessing and manipulating components.
2. di: Dependency providing classes using Dagger hilt.
3. ui: fragment and activities along with their corresponding ViewModel.




![](images/schéma.png)
    



License

Copyright 2021 Jérémy Sil.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
