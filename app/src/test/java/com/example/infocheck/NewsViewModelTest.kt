package com.example.infocheck.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.infocheck.MainCoroutineRule
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.models.News
import com.example.infocheck.datas.models.QueryInfo
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import net.bytebuddy.implementation.bytecode.Throw
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


class NewsViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var data : DatasRepository
    lateinit var viewModel: NewsListViewModel

    private val news = News().apply {
        title = "une news"
        description = "il était une fois une news de test"

    }
    private val news2 = News().apply {
        title = "une news 2"
        description = "il était une fois une news de test, le retour"

    }



    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = NewsListViewModel(data, testDispatcher)
    }

    @Test fun `download news ok`() = runBlockingTest {
        val flow = flow {
            emit( NewsListViewModel.NewsUiState.Success( arrayListOf(news, news2)) )
        }

        `when`(data.downloadNews(null,false)).thenReturn(flow)

        val results = mutableListOf<NewsListViewModel.NewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.downloadNews(false)

        assertEquals(NewsListViewModel.NewsUiState.Loading, results[0])
        assertEquals(NewsListViewModel.NewsUiState.Success( arrayListOf(news, news2)) , results[1])


        job.cancel()

    }



    @Test fun `download news no news`() = runBlockingTest {
        val flow = flow {
            emit( NewsListViewModel.NewsUiState.NoQuery )
        }

        `when`(data.downloadNews(null,false)).thenReturn(flow)

        val results = mutableListOf<NewsListViewModel.NewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.downloadNews(false)

        assertEquals(NewsListViewModel.NewsUiState.Loading, results[0])
        assertEquals(NewsListViewModel.NewsUiState.NoQuery, results[1])


        job.cancel()

    }


    @Test fun `download news error`() = runBlockingTest {

        var e = Exception()
        val flow = flow {
            throw  e
            emit( NewsListViewModel.NewsUiState.NoQuery )
        }

        `when`(data.downloadNews(null,false)).thenReturn(flow)

        val results = mutableListOf<NewsListViewModel.NewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.downloadNews(false)

        assertEquals(NewsListViewModel.NewsUiState.Loading, results[0])
        assertEquals(NewsListViewModel.NewsUiState.Error(e), results[1])


        job.cancel()

    }

}

