package com.example.infocheck


import com.example.infocheck.datas.Datas
import com.example.infocheck.datas.api.NewsListInterface
import com.example.infocheck.datas.models.News
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory




class NewsApiTest {


   lateinit var retrofit: Retrofit
   lateinit var  serviceListNews : NewsListInterface


    @Before
    fun init(){

        retrofit =  Retrofit.Builder()
            .baseUrl(Datas.BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        serviceListNews = retrofit.create(NewsListInterface::class.java)
    }


    @Test
    fun `getNewsFromApi ok`() = runBlocking {

        var list :  List<News> = emptyList()
        val response = serviceListNews.getNews("coronavirus", "2021-07-01", "fr", "publishedAt",
            Datas.APIKEY,5)
        if(response.isSuccessful){
            list =  response.body()?.articles!!
        }

        assertTrue(response.isSuccessful)
        assertEquals(list.size,5)


    }



    @Test
    fun `download image ok`() = runBlocking{


        var bmp : ByteArray? = null
        val response = serviceListNews.downloadImageWithDynamicUrl("https://developer.android.com/google-play/resources/icon-design-specifications/images/rounded-corners-freeform.png")
        if(response.isSuccessful){
            response.body()?.let {
                bmp = response.body()?.bytes()
            }

        }

        assertTrue(response.isSuccessful)
        assertNotNull(bmp)


    }


}
