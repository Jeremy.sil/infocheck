package com.example.infocheck.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.infocheck.MainCoroutineRule
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.models.News
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations



class NewsDetailViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var data : DatasRepository
    lateinit var viewModel: NewsDetailViewModel




    private val news = News().apply {
        title = "une news"
        description = "il était une fois une news de test"

    }




    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = NewsDetailViewModel(data, testDispatcher)
    }

    @Test fun `save news ok`() = runBlockingTest {
        val flow = flow {
            emit(NewsDetailViewModel.SaveState.Success )
        }

        `when`(data.saveNews(0)).thenReturn(flow)

        val results = mutableListOf<NewsDetailViewModel.SaveState>()

        val job = launch(testDispatcher) {
            viewModel.saveState.toList(results)
        }

        viewModel.saveNews(0)

        assertEquals(NewsDetailViewModel.SaveState.Empty, results[0])
        assertEquals(NewsDetailViewModel.SaveState.Success, results[1])


        job.cancel()

    }

    @Test fun `save news error`() = runBlockingTest {

        var e = Exception()
        val flow = flow {
            throw e
            emit(NewsDetailViewModel.SaveState.Success )
        }

        `when`(data.saveNews(0)).thenReturn(flow)

        val results = mutableListOf<NewsDetailViewModel.SaveState>()

        val job = launch(testDispatcher) {
            viewModel.saveState.toList(results)
        }

        viewModel.saveNews(0)

        assertEquals(NewsDetailViewModel.SaveState.Empty, results[0])
        assertEquals(NewsDetailViewModel.SaveState.Error(e), results[1])


        job.cancel()

    }


    @Test fun `save news already save`() = runBlockingTest {
        val flow = flow {
            emit(NewsDetailViewModel.SaveState.AlreadySaved )
        }

        `when`(data.saveNews(0)).thenReturn(flow)

        val results = mutableListOf<NewsDetailViewModel.SaveState>()

        val job = launch(testDispatcher) {
            viewModel.saveState.toList(results)
        }

        viewModel.saveNews(0)

        assertEquals(NewsDetailViewModel.SaveState.Empty, results[0])
        assertEquals(NewsDetailViewModel.SaveState.AlreadySaved, results[1])


        job.cancel()

    }

}

