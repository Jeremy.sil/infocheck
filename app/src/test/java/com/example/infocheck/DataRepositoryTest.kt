package com.example.infocheck.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.infocheck.MainCoroutineRule
import com.example.infocheck.datas.Datas
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.api.NewsListInterface
import com.example.infocheck.datas.api.NewsResponse
import com.example.infocheck.datas.db.NewsDao
import com.example.infocheck.datas.db.QueryDao
import com.example.infocheck.datas.models.News
import com.example.infocheck.datas.models.QueryInfo
import com.example.infocheck.ui.utils.ImageInterface
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import retrofit2.Response


class DataRepositoryTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var newsDao: NewsDao
    @Mock
    lateinit var queryDao: QueryDao
    @Mock
    lateinit var serviceListNews: NewsListInterface
    @Mock
    lateinit var bitmapUtils: ImageInterface

    lateinit var data : DatasRepository


    //Fake datas queries returned from dao
    var listQuery = arrayListOf<QueryInfo>(QueryInfo("coronavirus"),QueryInfo("Tokyo"))

    //fake news returned from dao
    var listNews = arrayListOf<News>(News().apply { title= " titre1"
        description = "descrption 1"
      },
            News().apply { title= " titre2"
        description = "descrption 2"
               })

    var listNews2 = arrayListOf<News>(News().apply { title= " titre1"
        description = "descrption 1"
        urlToImage="une url"},
        News().apply { title= " titre2"
            description = "descrption 2"
            urlToImage="une url"})

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        data = Datas(newsDao,queryDao,serviceListNews, bitmapUtils)

        //return fake datas from dao
        `when`(queryDao.getAllQueryInfo()).thenReturn(listQuery)
        `when`(newsDao.getAllNews()).thenReturn(listNews)


    }


    @Test fun `download news ok`()=
        runBlockingTest {


            `when`(serviceListNews.getNews(anyString(), anyString(), anyString(), anyString(), anyString(), eq(5))).thenReturn(
                retrofit2.Response.success( NewsResponse( ).apply { articles = listNews })
            )

            val results = mutableListOf<NewsListViewModel.NewsUiState>()

            val job = launch(testDispatcher) {
                data.downloadNews(null,false).toList(results)
            }

            assertEquals( (results[0] as NewsListViewModel.NewsUiState.Success).news.size,4 )

            job.cancel()


        }

    @Test fun `download news with image ok`()=
        runBlockingTest {


            `when`(serviceListNews.getNews(anyString(), anyString(), anyString(), anyString(), anyString(), eq(5))).thenReturn(
                Response.success( NewsResponse( ).apply { articles = listNews2 })
            )

            `when`(serviceListNews.downloadImageWithDynamicUrl(anyString())).thenReturn(
                Response.success(ResponseBody.create(null, byteArrayOf()))
            )

            val results = mutableListOf<NewsListViewModel.NewsUiState>()

            val job = launch(testDispatcher) {
                data.downloadNews(null,false).toList(results)
            }

            assertEquals(NewsListViewModel.NewsUiState.Success(anyList())::class, results[0]::class)
            assertEquals( (results[0] as NewsListViewModel.NewsUiState.Success).news.size,4 )


            job.cancel()


        }

    @Test fun `get saved news ok`()=

        runBlockingTest {


            val results = mutableListOf<List<News>>()

            val job = launch(testDispatcher) {
                data.getListSavedNews().toList(results)
            }

            assertEquals(listNews, results[0])


            job.cancel()


        }


}

