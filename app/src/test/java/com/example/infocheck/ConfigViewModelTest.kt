package com.example.infocheck.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.infocheck.MainCoroutineRule
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.models.QueryInfo
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations


class ConfigViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var data : DatasRepository
    lateinit var viewModel: ConfigViewModel

    private val queryInfo = QueryInfo("testquery")

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        val flow = flow {
            emit(arrayListOf(queryInfo) )
        }

        `when`(data.getAllQuery()).thenReturn(flow)

        runBlocking {


            viewModel = ConfigViewModel(data, testDispatcher)

        }


    }


    @Test fun AddQuery()=
        runBlockingTest {

            val flow = flow {
                emit(arrayListOf(queryInfo) )
            }

            `when`(data.addQuery(queryInfo)).thenReturn(flow)

            val results = mutableListOf<ConfigViewModel.QueryUiState>()

            val job = launch(testDispatcher) {
                viewModel.listQueryInfo.toList(results)
            }

            viewModel.addQuery(queryInfo)
            assertEquals(ConfigViewModel.QueryUiState.Success(arrayListOf(queryInfo)), results[0])

            assertEquals(ConfigViewModel.QueryUiState.Loading, results[1])
            assertEquals(ConfigViewModel.QueryUiState.Success(arrayListOf(queryInfo)), results[2])


            job.cancel()


        }


    @Test fun `AddQuery error`()=
        runBlockingTest {

            val e = java.lang.Exception()
            val flow = flow {
               throw e
                emit(arrayListOf(queryInfo) )
            }

            `when`(data.addQuery(queryInfo)).thenReturn(flow)

            val results = mutableListOf<ConfigViewModel.QueryUiState>()

            val job = launch(testDispatcher) {
                viewModel.listQueryInfo.toList(results)
            }

            viewModel.addQuery(queryInfo)
            assertEquals(ConfigViewModel.QueryUiState.Success(arrayListOf(queryInfo)), results[0])

            assertEquals(ConfigViewModel.QueryUiState.Loading, results[1])
            assertEquals(ConfigViewModel.QueryUiState.Error(e), results[2])


            job.cancel()


        }

    @Test fun `AddQuery bad query`()=
        runBlockingTest {

            val e = java.lang.Exception()
            val flow = flow {
                throw e
                emit(arrayListOf(queryInfo) )
            }

            `when`(data.addQuery(queryInfo)).thenReturn(flow)

            val results = mutableListOf<ConfigViewModel.QueryUiState>()

            val job = launch(testDispatcher) {
                viewModel.listQueryInfo.toList(results)
            }

            viewModel.addQuery(QueryInfo("queryInfo &²"))
            assertEquals(ConfigViewModel.QueryUiState.Success(arrayListOf(queryInfo)), results[0])

            assertEquals(ConfigViewModel.QueryUiState.Loading, results[1])
            assertEquals(ConfigViewModel.QueryUiState.BadQuery, results[2])


            job.cancel()


        }


    @Test fun `remove query ok noquery`()=
        runBlockingTest {


            val flow = flow {
                emit(ArrayList<QueryInfo>() )
            }

            `when`(data.removeQuery(queryInfo)).thenReturn(flow)

            val results = mutableListOf<ConfigViewModel.QueryUiState>()

            val job = launch(testDispatcher) {
                viewModel.listQueryInfo.toList(results)
            }

            viewModel.removeQuery(queryInfo)
            assertEquals(ConfigViewModel.QueryUiState.Success(arrayListOf(queryInfo)), results[0])

            assertEquals(ConfigViewModel.QueryUiState.Loading, results[1])
            assertEquals(ConfigViewModel.QueryUiState.NoQuery, results[2])


            job.cancel()


        }


    @Test fun `remove query ok`()=
        runBlockingTest {


            val flow = flow {
                emit(arrayListOf(queryInfo) )
            }

            `when`(data.removeQuery(queryInfo)).thenReturn(flow)

            val results = mutableListOf<ConfigViewModel.QueryUiState>()

            val job = launch(testDispatcher) {
                viewModel.listQueryInfo.toList(results)
            }

            viewModel.removeQuery(queryInfo)
            assertEquals(ConfigViewModel.QueryUiState.Success(arrayListOf(queryInfo)), results[0])

            assertEquals(ConfigViewModel.QueryUiState.Loading, results[1])
            assertEquals(ConfigViewModel.QueryUiState.Success(arrayListOf(queryInfo)), results[2])


            job.cancel()


        }


    @Test fun `remove query error`()=
        runBlockingTest {

            val e = Exception()
            val flow = flow {
               throw e
                emit(arrayListOf(queryInfo) )
            }

            `when`(data.removeQuery(queryInfo)).thenReturn(flow)

            val results = mutableListOf<ConfigViewModel.QueryUiState>()

            val job = launch(testDispatcher) {
                viewModel.listQueryInfo.toList(results)
            }

            viewModel.removeQuery(queryInfo)
            assertEquals(ConfigViewModel.QueryUiState.Success(arrayListOf(queryInfo)), results[0])

            assertEquals(ConfigViewModel.QueryUiState.Loading, results[1])
            assertEquals(ConfigViewModel.QueryUiState.Error(e), results[2])


            job.cancel()


        }
}

