package com.example.infocheck.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.infocheck.MainCoroutineRule
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.models.News
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations


class SaveViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var data : DatasRepository
    lateinit var viewModel: SaveViewModel

    private val news = News().apply {
        title = "une news"
        description = "il était une fois une news de test"

    }
    private val news2 = News().apply {
        title = "une news 2"
        description = "il était une fois une news de test, le retour"

    }



    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = SaveViewModel(data, testDispatcher)
    }

    @Test fun `load news ok`() = runBlockingTest {
        val flow = flow {
            emit(arrayListOf(news, news2) )
        }

        `when`(data.getListSavedNews()).thenReturn(flow)

        val results = mutableListOf<SaveViewModel.SaveNewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.loadNews()

        assertEquals(SaveViewModel.SaveNewsUiState.Loading, results[0])
        assertEquals(SaveViewModel.SaveNewsUiState.Success( arrayListOf(news, news2)) , results[1])


        job.cancel()

    }


    @Test fun `loadnews news nosave`() = runBlockingTest {
        val flow = flow {
            emit(ArrayList<News>() )
        }

        `when`(data.getListSavedNews()).thenReturn(flow)

        val results = mutableListOf<SaveViewModel.SaveNewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.loadNews()

        assertEquals(SaveViewModel.SaveNewsUiState.Loading, results[0])
        assertEquals(SaveViewModel.SaveNewsUiState.NoSaves , results[1])


        job.cancel()

    }


    @Test fun `loadnews error`() = runBlockingTest {

        var e = Exception()
        val flow = flow {
            throw  e
            emit(ArrayList<News>() )
        }

        `when`(data.getListSavedNews()).thenReturn(flow)

        val results = mutableListOf<SaveViewModel.SaveNewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.loadNews()

        assertEquals(SaveViewModel.SaveNewsUiState.Loading, results[0])
        assertEquals(SaveViewModel.SaveNewsUiState.Error(e) , results[1])


        job.cancel()

    }





    @Test fun `delete news ok`() = runBlockingTest {
        val flow = flow {
            emit(arrayListOf(news, news2) )
        }

        `when`(data.deleteNews(1)).thenReturn(flow)

        val results = mutableListOf<SaveViewModel.SaveNewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.deleteNews(1)

        assertEquals(SaveViewModel.SaveNewsUiState.Loading, results[0])
        assertEquals(SaveViewModel.SaveNewsUiState.Success( arrayListOf(news, news2)) , results[1])


        job.cancel()

    }



    @Test fun `deletenews news nosave`() = runBlockingTest {
        val flow = flow {
            emit(ArrayList<News>() )
        }

        `when`(data.deleteNews(1)).thenReturn(flow)

        val results = mutableListOf<SaveViewModel.SaveNewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.deleteNews(1)

        assertEquals(SaveViewModel.SaveNewsUiState.Loading, results[0])
        assertEquals(SaveViewModel.SaveNewsUiState.NoSaves , results[1])


        job.cancel()

    }


    @Test fun `delete news error`() = runBlockingTest {

        var e = Exception()
        val flow = flow {
            throw  e
            emit(ArrayList<News>() )
        }

        `when`(data.deleteNews(1)).thenReturn(flow)

        val results = mutableListOf<SaveViewModel.SaveNewsUiState>()

        val job = launch(testDispatcher) {
            viewModel.listNews.toList(results)
        }

        viewModel.deleteNews(1)

        assertEquals(SaveViewModel.SaveNewsUiState.Loading, results[0])
        assertEquals(SaveViewModel.SaveNewsUiState.Error(e) , results[1])


        job.cancel()

    }
}

