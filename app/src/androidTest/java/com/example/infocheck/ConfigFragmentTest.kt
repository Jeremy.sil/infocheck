package com.example.infocheck


import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.test.runner.AndroidJUnit4
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.ui.fragments.ConfigFragment
import com.example.infocheck.ui.viewmodel.ConfigViewModel
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations


@RunWith(AndroidJUnit4::class)
class ConfigFragmentTest {


//@Mock
lateinit var configViewModel : ConfigViewModel
    @Mock
    lateinit var data : DatasRepository


    @Before
    fun init(){

        MockitoAnnotations.initMocks(this)

        val flow = flow {
            emit(arrayListOf(ConfigViewModel.QueryUiState.Loading) )
        }

        //Mockito.`when`(configViewModel.listQueryInfo.).thenReturn(flow)
        //val appContext = androidx.test.platform.app.InstrumentationRegistry.getInstrumentation().context

        val fragmentArgs = bundleOf("numElements" to 0)
        //val scenario= launchFragment<ConfigFragment>()
        val viewModel = ConfigViewModel(data)

        val scenario = launchFragmentInHiltContainer<ConfigFragment>  (){
            ConfigFragment().apply {
                //appExecutors = countingAppExecutors.appExecutors
                configViewModel =  viewModel
            }

        }


    }


    @Test
    fun getNews() {



    }




    open class InjectedViewModel : ViewModel() {
        /**
         * This would normally be more complicated logic or return a LiveData instance
         */
        open fun getUserName() = ""
    }
    class InjectedViewModelFactoryFragment(
        private val viewModelFactory: ViewModelProvider.Factory
    ) : Fragment() {
        val viewModel: InjectedViewModel by viewModels { viewModelFactory }



}

}
