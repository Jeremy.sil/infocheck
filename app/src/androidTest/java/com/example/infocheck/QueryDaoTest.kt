package com.example.infocheck

import androidx.room.Room
import androidx.test.runner.AndroidJUnit4
import com.example.infocheck.datas.db.AppDatabase
import com.example.infocheck.datas.db.QueryDao
import com.example.infocheck.datas.models.QueryInfo
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class QueryDaoTest {


    lateinit var database: AppDatabase
    lateinit var queryDao : QueryDao

    var listQuery = arrayListOf<QueryInfo>(
       QueryInfo("query1"),
       QueryInfo("query2")
        )

    val queryInfo = QueryInfo("query3")

    @Before
    fun init(){

        val appContext = androidx.test.platform.app.InstrumentationRegistry.getInstrumentation().context

         database = Room.inMemoryDatabaseBuilder(
            appContext,
            AppDatabase::class.java

        ).build()

        queryDao = database.queryDao()
        listQuery.forEach {
            queryDao.insertAllQueryInfo(it)
        }

    }


    @Test
    fun getAllQueries() {

        var list  = queryDao.getAllQueryInfo()

        assertTrue(listQuery.size == list.size)
       assertTrue( listQuery.zip(list).all { (x, y) -> x.queryName == y.queryName  })


    }

    @Test
    fun addQuery() {


        queryDao.insertAllQueryInfo(queryInfo)
        listQuery.add(queryInfo)
        var list  = queryDao.getAllQueryInfo()

        assertTrue(listQuery.size == list.size)
        assertTrue( listQuery.zip(list).all { (x, y) -> x.queryName == y.queryName  })


    }


    @Test
    fun removeQuery() {

        listQuery.remove(queryInfo)
        queryDao.delete(queryInfo)

        var list  = queryDao.getAllQueryInfo()

        assertTrue(listQuery.size == list.size)
        assertTrue( listQuery.zip(list).all { (x, y) -> x.queryName == y.queryName  })


    }
}
