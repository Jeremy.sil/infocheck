package com.example.infocheck

import android.content.Context
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.example.infocheck.datas.db.AppDatabase
import com.example.infocheck.datas.db.NewsDao
import com.example.infocheck.datas.models.News
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class NewsDaoTest {


    lateinit var database: AppDatabase
    lateinit var newsDao : NewsDao

    var listNews = arrayListOf<News>(
        News().apply { title= " titre1"
        description = "descrption 1"
    },
        News().apply { title= " titre2"
            description = "descrption 2"
        })

    val news = News().apply {
        title = "an other news"
        author= "Bob"
    }

    @Before
    fun init(){

        val appContext = androidx.test.platform.app.InstrumentationRegistry.getInstrumentation().context

         database = Room.inMemoryDatabaseBuilder(
            appContext,
            AppDatabase::class.java

        ).build()

        newsDao = database.newsDao()
        listNews.forEach {
            newsDao.insertAllNews(it)
        }

    }


    @Test
    fun getNews() {

        var list  = newsDao.getAllNews()

        assertTrue(listNews.size == list.size)
       assertTrue( listNews.zip(list).all { (x, y) -> x.title == y.title && x.author == y.author })


    }

    @Test
    fun addNews() {

        listNews.add(news)
        newsDao.insertAllNews(news)

        var list  = newsDao.getAllNews()

        assertTrue(listNews.size == list.size)
        assertTrue( listNews.zip(list).all { (x, y) -> x.title == y.title && x.author == y.author })


    }


    @Test
    fun deleteNews() {



        listNews.remove(news)
        newsDao.delete(news)

        var list  = newsDao.getAllNews()

        assertTrue(listNews.size == list.size)
        assertTrue( listNews.zip(list).all { (x, y) -> x.title == y.title && x.author == y.author })


    }

}
