package com.example.infocheck

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import com.example.infocheck.R
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.example.infocheck.ui.MainActivity
import org.junit.Rule
import org.junit.Test
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.hamcrest.CoreMatchers.*
import org.hamcrest.Matcher
import org.hamcrest.Matchers.hasEntry
import androidx.test.espresso.contrib.RecyclerViewActions

class NavigationUITest {

    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(MainActivity::class.java)



    @Test
    fun NavigateConfig() {
        onView(withId(R.id.recycler_news)).check(matches(isDisplayed()))
        onView(withId(R.id.configFragment)).perform(click())
        onView(withId(R.id.recycler_query)).check(matches(isDisplayed()))

    }

    @Test
    fun NavigateSaves() {
        onView(withId(R.id.saveFragment)).perform(click())
        onView(withId(R.id.recycler_news)).check(matches(isDisplayed()))

    }

    @Test
    fun NavigateListNews() {
        onView(withId(R.id.configFragment)).perform(click())
        onView(withId(R.id.recycler_query)).check(matches(isDisplayed()))
        onView(withId(R.id.newsListFragment)).perform(click())
        onView(withId(R.id.recycler_news)).check(matches(isDisplayed()))
    }


    @Test
    fun NavigateDetails() {

        onView(withId(R.id.recycler_news)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ClickOnButtonView()))
        onView(withId(R.id.image_news_details)).check(matches(isDisplayed()))

    }

    //perfom click on recyclerview item
    inner class ClickOnButtonView : ViewAction {
        internal var click = ViewActions.click()

        override fun getConstraints(): Matcher<View> {
            return click.constraints
        }

        override fun getDescription(): String {
            return " click on custom button view"
        }

        override fun perform(uiController: UiController, view: View) {
            //btnClickMe -> Custom row item view button
            click.perform(uiController, view)
        }
    }
}