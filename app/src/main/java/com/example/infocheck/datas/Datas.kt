package com.example.infocheck.datas





import com.example.infocheck.datas.api.NewsListInterface
import com.example.infocheck.datas.db.NewsDao
import com.example.infocheck.datas.db.QueryDao
import com.example.infocheck.datas.models.News
import com.example.infocheck.datas.models.QueryInfo
import com.example.infocheck.ui.utils.ImageInterface
import com.example.infocheck.ui.viewmodel.NewsDetailViewModel
import com.example.infocheck.ui.viewmodel.NewsListViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.BufferedInputStream
import javax.inject.Inject


class Datas  @Inject constructor(
    private val newsDao: NewsDao,
    private val queryDao: QueryDao,
    private val serviceListNews: NewsListInterface,
    private  val bitmapUtils: ImageInterface
) : DatasRepository {



    var listNews : ArrayList<News> = ArrayList()
    var listSavedNews : ArrayList<News> = ArrayList()

    companion object {
        const val APIKEY = "eb1151afccb34be890ee835b94563c26"
        const val BASEURL = "http://newsapi.org/v2/"
    }




    private var updated : Boolean = false
    private var downloadJob : Job? = null


    init {



}

    override fun getAllQuery() : Flow<List<QueryInfo>> = flow {
       emit( queryDao.getAllQueryInfo())
    }



    override fun saveNews(position: Int ) : Flow<NewsDetailViewModel.SaveState> = flow {


        listNews.get(position).let {
            if (newsDao.getAllNews().any{ News -> News.title ==  it.title }){
               emit(NewsDetailViewModel.SaveState.AlreadySaved)
            }else{
                newsDao.insertAllNews(it)
                emit(NewsDetailViewModel.SaveState.Success)
            }

        }

    }



    override fun addQuery(queryInfo: QueryInfo) : Flow<ArrayList<QueryInfo>> = flow {

        queryDao.insertAllQueryInfo(queryInfo)
        val listQueryInfo = queryDao.getAllQueryInfo() as ArrayList<QueryInfo>
        stopAndClearNews()
        emit(listQueryInfo)

    }


    override fun removeQuery(queryInfo : QueryInfo) : Flow<ArrayList<QueryInfo>> = flow {

        queryDao.delete(queryInfo)
        val listQueryInfo = queryDao.getAllQueryInfo() as ArrayList<QueryInfo>
        stopAndClearNews()
        emit(listQueryInfo)

    }

    private  fun stopAndClearNews(){
        updated = false
        GlobalScope.launch {
            downloadJob?.cancelAndJoin()
            listNews.clear()
        }


    }

     override fun getListSavedNews(): Flow<List<News>> = flow {


         listSavedNews = newsDao.getAllNews() as ArrayList<News>

         for (news in listSavedNews) {
             if (news.imageNews == null) {
                 news.imageArray?.let {
                     val bufferedInputStream = BufferedInputStream(it.inputStream())

                     news.imageNews = bitmapUtils.decodeStream(bufferedInputStream)
                 }
             }

             emit(listSavedNews)


         }
     }

    override fun getNews(position: Int) =  listNews.get(position)

    override fun getSavedNews(position: Int) = listSavedNews.get(position)


    override fun deleteNews(position: Int): Flow<List<News>> = flow  {
       newsDao.delete(listSavedNews.get(position))
        //listSavedNews = newsDao.getAllNews() as ArrayList<News>
        listSavedNews.remove(listSavedNews.get(position))
        emit(listSavedNews)

    }




    private suspend fun downloadImage(url : String): ByteArray? {

        var bmp : ByteArray? = null
        val response = serviceListNews.downloadImageWithDynamicUrl(url)
        if(response.isSuccessful){
            response.body()?.let {
                bmp = response.body()?.bytes()
            }

        }

        return  bmp
    }



    private suspend fun downloadQuery(queryInfo: QueryInfo) : ArrayList<News>{


            val response = serviceListNews.getNews(queryInfo.queryName, "2021-07-01", "fr", "publishedAt", APIKEY,5)
            if(response.isSuccessful){
                return  response.body()?.articles!!
            }else{
                throw  Exception("Server response error")
            }

    }



    override  fun downloadNews(downloadJb: Job?, forceDownload: Boolean): Flow<NewsListViewModel.NewsUiState>  = flow {

        var  tmpListNews : ArrayList<News> = ArrayList()

        if (forceDownload) {
           stopAndClearNews()
        }
        downloadJob = downloadJb
        if(!updated){
                val listQueryInfo = queryDao.getAllQueryInfo()
                if(listQueryInfo.isNotEmpty()){
                    listQueryInfo.forEach(){
                        val list = downloadQuery(it)
                        tmpListNews.addAll(list )
                        listNews.addAll( list)

                    }
                    emit(NewsListViewModel.NewsUiState.Success( listNews))
                    updated = true
                    for (news in tmpListNews){
                        news.urlToImage?.let {
                            news.imageArray =  downloadImage(it)
                            news.imageNews =  bitmapUtils.decodeStream(BufferedInputStream( news.imageArray?.inputStream()))
                            emit(NewsListViewModel.NewsUiState.Success( listNews))
                        }

                    }

                }else {
                    emit(NewsListViewModel.NewsUiState.NoQuery)

                }

            } else{
                 emit(NewsListViewModel.NewsUiState.Success( listNews))


            }


    }

}



