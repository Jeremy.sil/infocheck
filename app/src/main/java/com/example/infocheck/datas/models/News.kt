package com.example.infocheck.datas.models

import android.graphics.Bitmap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class
News {
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0

    @ColumnInfo(name = "author")  @SerializedName("author")
    var author: String? = null
    @ColumnInfo(name = "title")  @SerializedName("title")
    var title: String? = null
    @ColumnInfo(name = "description") @SerializedName("description")
    var description : String? = null
    @ColumnInfo(name = "urlToImage")  @SerializedName("urlToImage")
    var urlToImage : String? = null
    @ColumnInfo(name = "publishedAt") @SerializedName("publishedAt")
    var publishedAt : String? = null
    @ColumnInfo(name = "content") @SerializedName("content")
    var content : String? = null
    @ColumnInfo(name = "url") @SerializedName("url")
    var url : String? = null
//    @ColumnInfo(name = "imageNews")
    @Ignore
    var imageNews : Bitmap? = null
    @ColumnInfo(name = "imageArray")
    var imageArray : ByteArray? = null

}