package com.example.infocheck.datas.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.infocheck.datas.db.NewsDao
import com.example.infocheck.datas.models.News
import com.example.infocheck.datas.models.QueryInfo


@Database(entities = arrayOf(News::class, QueryInfo::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
    abstract fun queryDao(): QueryDao
}
