package com.example.infocheck.datas.api

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url


interface NewsListInterface {
    @GET("everything")
    suspend fun  getNews(
        @Query("q") typeQ: String,
        @Query("from") from: String,
        @Query("language") language: String,
        @Query("sortby") sortby: String,
        @Query("apiKey") apikey: String,
        @Query("pageSize") pageSize: Int,

    ): Response<NewsResponse>

    @GET
    suspend fun downloadImageWithDynamicUrl(@Url imageUrl: String): Response<ResponseBody?>

}
