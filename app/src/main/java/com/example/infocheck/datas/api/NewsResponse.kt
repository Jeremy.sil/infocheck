package com.example.infocheck.datas.api


import com.example.infocheck.datas.models.News
import com.google.gson.annotations.SerializedName

class NewsResponse {

    @SerializedName("articles")
    var articles = ArrayList<News>()

}


