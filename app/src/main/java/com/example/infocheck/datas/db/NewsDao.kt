package com.example.infocheck.datas.db

import androidx.room.*
import com.example.infocheck.datas.models.News
import com.example.infocheck.datas.models.QueryInfo

@Dao
interface NewsDao {
    @Query("SELECT * FROM news")
    fun getAllNews():   List<News>

    @Query("SELECT * FROM news WHERE uid IN (:id)")
    fun loadAllNewsByIds(id: IntArray): List<News>

    //@Query("SELECT * FROM news WHERE first_name LIKE :first AND " + "last_name LIKE :last LIMIT 1")
    //fun findByName(first: String, last: String): News

    @Insert
    fun insertAllNews(vararg news : News)

    @Delete
    fun delete(news : News)


}
