package com.example.infocheck.datas.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class QueryInfo ( @ColumnInfo(name = "queryName")
                  var queryName: String) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0


    @ColumnInfo(name = "color")
    var color: String? = null



}