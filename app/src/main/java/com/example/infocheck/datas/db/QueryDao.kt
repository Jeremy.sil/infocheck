package com.example.infocheck.datas.db

import androidx.room.*
import com.example.infocheck.datas.models.QueryInfo

@Dao
interface QueryDao {

    @Query("SELECT * FROM queryinfo")
    fun getAllQueryInfo():  List<QueryInfo>

    @Query("SELECT * FROM queryinfo WHERE id IN (:id)")
    fun loadAllQueryInfoByIds(id: IntArray): List<QueryInfo>

    @Update
    fun updateQueryInfo(queryInfo: QueryInfo)

    @Insert
    fun insertAllQueryInfo(vararg queryInfo: QueryInfo)

    @Delete
    fun delete(queryInfo: QueryInfo

    )
}
