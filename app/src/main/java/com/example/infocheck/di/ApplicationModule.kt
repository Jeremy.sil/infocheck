package com.example.infocheck.di

import android.content.Context
import androidx.room.Room
import com.example.infocheck.datas.db.AppDatabase
import com.example.infocheck.datas.Datas
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.api.NewsListInterface
import com.example.infocheck.ui.utils.BitmapUtils
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {



    @Singleton
    @Provides
    fun provideDataBase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        AppDatabase::class.java, "database-name"

    ).build()

    @Singleton
    @Provides
    fun provideNewsDao(db: AppDatabase) = db.newsDao()

    @Singleton
    @Provides
    fun provideQueryDao(db: AppDatabase) = db.queryDao()

    @Provides
    @Singleton
    fun provideDatasRepository(@ApplicationContext context : Context) : DatasRepository{

        return  Datas(provideNewsDao(provideDataBase(context)), provideQueryDao(provideDataBase(context)),provideRetrofitNewsInterface(provideRetrofit()),provideBitmapUtils())
    }


    @Provides
    @Singleton
    fun provideRetrofit()  : Retrofit {
        return Retrofit.Builder()
            .baseUrl(Datas.BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofitNewsInterface(retrofit: Retrofit) =
    retrofit.create(NewsListInterface::class.java)

    @Singleton
    @Provides
    fun provideBitmapUtils() =
        BitmapUtils()




}