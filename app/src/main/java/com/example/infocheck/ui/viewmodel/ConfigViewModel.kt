package com.example.infocheck.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.models.QueryInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject


// ConfigviewModel holds datas for configFragment
@HiltViewModel
class ConfigViewModel( private val repository : DatasRepository, private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : ViewModel()
//for testing purposes to override the dispatcher
{ @Inject constructor(repos : DatasRepository) : this(repos, Dispatchers.IO)


    private val _listQueryInfo: MutableStateFlow<QueryUiState> = MutableStateFlow(QueryUiState.Loading)
    val listQueryInfo: StateFlow<QueryUiState> = _listQueryInfo



    init {

        // get queries recording in db from repository and update stateflow
        repository.getAllQuery().onEach {
            if(it.isNotEmpty()){
                _listQueryInfo.value = QueryUiState.Success(it)
            }else{
                _listQueryInfo.value = QueryUiState.NoQuery
            }
        }
            .catch {  e->
                _listQueryInfo.value = QueryUiState.Error(e)
            }.flowOn(dispatcher)
            .launchIn(viewModelScope)


    }


//add query to repository db and update state flow
    fun addQuery(queryInfo: QueryInfo) {

        _listQueryInfo.value = QueryUiState.Loading
    if(isQueryValid(queryInfo)){
        repository.addQuery(queryInfo).onEach {
            _listQueryInfo.value = QueryUiState.Success(it)
        }.catch {e->
            _listQueryInfo.value = QueryUiState.Error(e)
        }.flowOn(dispatcher)
            .launchIn(viewModelScope)

    }else
    {
        _listQueryInfo.value = QueryUiState.BadQuery
        _listQueryInfo.value = QueryUiState.Loading
    }


    }


    private fun isQueryValid(queryInfo : QueryInfo) : Boolean{
        if (queryInfo.queryName.isEmpty())
            return  false
        if (!queryInfo.queryName.matches(Regex("[a-zA-Z. ]*")))
            return false
        return  true
    }

//remove query from repository db and update stateflow
    fun removeQuery(queryInfo: QueryInfo) {
        _listQueryInfo.value = QueryUiState.Loading
        repository.removeQuery(queryInfo).onEach {
            if(it.isEmpty()){
                _listQueryInfo.value = QueryUiState.NoQuery
            }else{
                _listQueryInfo.value = QueryUiState.Success(it)
            }
        }.catch {e->
            _listQueryInfo.value = QueryUiState.Error(e)
        }.flowOn(dispatcher)
            .launchIn(viewModelScope)

    }

//handle query ui state
    sealed class QueryUiState {
        data class Success(val listQuery: List<QueryInfo>) : QueryUiState()
        data class Error(val exception: Throwable) : QueryUiState()
        object BadQuery : QueryUiState()
        object NoQuery : QueryUiState()
        object Loading : QueryUiState()
    }
}