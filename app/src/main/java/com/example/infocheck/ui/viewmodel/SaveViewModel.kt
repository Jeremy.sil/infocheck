package com.example.infocheck.ui.viewmodel



import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.models.News
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class SaveViewModel( private val repository : DatasRepository, private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : ViewModel()
//for testing purposes to override the dispatcher
{ @Inject constructor(repos : DatasRepository) : this(repos, Dispatchers.IO)

//contains list of saved news
    private val _listNews: MutableStateFlow<SaveNewsUiState> = MutableStateFlow(SaveNewsUiState.Loading)
    val listNews: StateFlow<SaveNewsUiState> = _listNews

//load news from db repository
   fun loadNews() {

       _listNews.value = SaveNewsUiState.Loading
       repository.getListSavedNews().onEach { it ->
           if(!it.isEmpty()){
               _listNews.value = SaveNewsUiState.Success(it)
           }else{
               _listNews.value = SaveNewsUiState.NoSaves
           }
       }.catch { e->
           _listNews.value = SaveNewsUiState.Error(e)
       }.flowOn(dispatcher)
           .launchIn(viewModelScope)
    }

    //delete news from db repository
    fun deleteNews(position : Int){
        _listNews.value = SaveNewsUiState.Loading
        repository.deleteNews(position).onEach {
            if(!it.isEmpty()){
                _listNews.value = SaveNewsUiState.Success(it)
            }else{
                _listNews.value = SaveNewsUiState.NoSaves
            }

        }.catch { e->
            _listNews.value = SaveNewsUiState.Error(e)
        }.flowOn(dispatcher)
            .launchIn(viewModelScope)

    }


    sealed class SaveNewsUiState {
        data class Success(val news:  List<News>): SaveNewsUiState()
        object Loading : SaveNewsUiState()
        object NoSaves : SaveNewsUiState()
        data class Error(val exception: Throwable): SaveNewsUiState()
    }

}