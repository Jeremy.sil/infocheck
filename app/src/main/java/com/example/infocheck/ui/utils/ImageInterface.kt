package com.example.infocheck.ui.utils

import android.graphics.Bitmap
import java.io.InputStream

interface ImageInterface
{
    fun decodeStream(bufferedInputStream : InputStream) : Bitmap


}