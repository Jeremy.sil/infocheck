package com.example.infocheck.ui.utils


import android.graphics.BitmapFactory

import java.io.InputStream


class BitmapUtils : ImageInterface {

    override fun decodeStream(bufferedInputStream : InputStream) = BitmapFactory.decodeStream(bufferedInputStream)

}