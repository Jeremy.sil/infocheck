package com.example.infocheck.ui.adapters

interface RecyclerInteractor {

    fun rowClicked( number: Int)
    fun rowClickedLong(number: Int)

}