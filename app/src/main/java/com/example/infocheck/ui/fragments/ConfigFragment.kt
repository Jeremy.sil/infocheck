package com.example.infocheck.ui.fragments


import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.RecyclerView
import com.example.infocheck.R
import com.example.infocheck.databinding.ConfigFragmentBinding
import com.example.infocheck.datas.models.QueryInfo
import com.example.infocheck.ui.viewmodel.ConfigViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


//Show all queries recorded in db, add and remove queries
@AndroidEntryPoint
class ConfigFragment( ) : Fragment( ) {


    private var _binding: ConfigFragmentBinding? = null
    private val binding get() = _binding!!

    private val configViewModel : ConfigViewModel by viewModels()
    val configFragment  =this

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ConfigFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.config_fragment_title)

//listen to viewmodel state flow nad update UI according to
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                configViewModel.listQueryInfo.collect() {
                    when (it) {
                        is ConfigViewModel.QueryUiState.Success -> {
                            binding.tvAddQueryBy.visibility = View.INVISIBLE
                            binding.recyclerQuery.adapter =
                                RecyclerAdapterConfig(it.listQuery, configFragment)

                        }
                        is ConfigViewModel.QueryUiState.Error -> {
                            Toast.makeText(
                                context,
                                getString(R.string.error) + it.exception.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        is ConfigViewModel.QueryUiState.BadQuery -> {
                            showAlert(getString(R.string.config_bad_query))
                        }
                        is ConfigViewModel.QueryUiState.NoQuery -> {
                            binding.recyclerQuery.adapter = null
                            binding.tvAddQueryBy.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }

//show the dialog to add a new query
       binding.buttonAddQuery.setOnClickListener{

            val alertDialog: AlertDialog? = activity?.let {
                val editText  = EditText(context)

                val builder = AlertDialog.Builder(it)
                builder.apply {
                    setView(editText)
                    setTitle(getString(R.string.config_enter_query))
                    setPositiveButton(R.string.ok,
                        { dialog, id ->
                            val queryInfo = QueryInfo(editText.text.toString())
                            configViewModel.addQuery(queryInfo)

                        })
                    setNegativeButton(getString(R.string.cancel),
                        { dialog, id ->
                            // User cancelled the dialog
                        })
                }

                builder.create()
            }
            alertDialog?.show()

        }

    }

    fun showAlert(message : String){
        val alertDialog: AlertDialog? = activity?.let {


            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle(getString(R.string.error))
                setMessage(message)
                setPositiveButton(R.string.ok
                ) { dialog, id ->


                }
            }

            builder.create()
        }
        alertDialog?.show()
    }

    fun rowClicked(query : QueryInfo){

        configViewModel.removeQuery(query)
    }



    private class RecyclerAdapterConfig(private val listQueryInfo:  List<QueryInfo>?, val configFragment: ConfigFragment) : RecyclerView.Adapter<RecyclerAdapterConfig.NewsHolder>()  {

        override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): NewsHolder {
            val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_query, viewGroup, false)

            return NewsHolder(view,this)

        }


        override fun getItemCount(): Int {
         return  listQueryInfo?.let { listQueryInfo.size } ?: 0
        }

        override fun onBindViewHolder(view: NewsHolder, number: Int) {
          listQueryInfo?.let {

              view.updateNews(listQueryInfo.get(number)) }
        }


        class NewsHolder(view: View,val adapter : RecyclerAdapterConfig) : RecyclerView.ViewHolder(view) {

            private val tv_query: TextView
            private val button_delete : ImageButton

            init {
                tv_query = view.findViewById(R.id.tv_query)
                button_delete = view.findViewById(R.id.button_delete)
            }

            fun updateNews(query : QueryInfo) {
              tv_query.setText(query.queryName)
                button_delete.setOnClickListener{

                    adapter.configFragment.rowClicked(query)

                }
            }

        }
    }



}
