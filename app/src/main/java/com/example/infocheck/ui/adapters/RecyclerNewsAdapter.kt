package com.example.infocheck.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.infocheck.R
import com.example.infocheck.datas.models.News

class RecyclerNewsAdapter(private var listNews: List<News>?, private val interactor: RecyclerInteractor) : RecyclerView.Adapter<RecyclerNewsAdapter.NewsHolder>()  {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): NewsHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_news, viewGroup, false)

        return NewsHolder(view)

    }

    fun setList(list : List<News>){
        this.listNews = list
    }


    override fun getItemCount(): Int {
        return  listNews?.let { it.size } ?: 0
    }

    override fun onBindViewHolder(view: NewsHolder, number: Int) {
        listNews?.let {
            view.itemView.setOnClickListener { view ->
                interactor.rowClicked(number)
            }
            view.itemView.setOnLongClickListener() { view ->
                interactor.rowClickedLong(number)
                true
            }
            view.updateNews(it.get(number))
        }
    }



    class NewsHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val tv_title: TextView
        private val tv_desc: TextView
        private val image_news: ImageView

        init {
            // Define click listener for the ViewHolder's View.
            tv_title = view.findViewById(R.id.tv_title)
            tv_desc = view.findViewById(R.id.tv_desc)
            image_news = view.findViewById((R.id.image_news))
        }

        fun updateNews(news : News) {
            tv_title.text = news.title
            tv_desc.text = news.description
            image_news.setImageBitmap(news.imageNews)
        }

    }
}