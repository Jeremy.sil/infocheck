package com.example.infocheck.ui.fragments

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.infocheck.R
import com.example.infocheck.databinding.SaveFragmentBinding
import com.example.infocheck.ui.NewsDetailsActivity
import com.example.infocheck.ui.adapters.RecyclerInteractor
import com.example.infocheck.ui.adapters.RecyclerNewsAdapter
import com.example.infocheck.ui.viewmodel.SaveViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


@AndroidEntryPoint
class SaveFragment : Fragment(), RecyclerInteractor {

    private var _binding: SaveFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SaveViewModel by viewModels()
    private val saveFragment = this

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SaveFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.save_fragemnt_title)

        lifecycleScope.launch {
             lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED){
                 viewModel.listNews.collect  {

                     when (it) {
                         is  SaveViewModel.SaveNewsUiState.Success ->{
                             if(!it.news.isEmpty()){
                                 binding.recyclerNews.adapter = RecyclerNewsAdapter(it.news  ,saveFragment)
                                 binding.tvFindSaveHere.visibility = View.INVISIBLE
                                 binding.progressBar.visibility = View.INVISIBLE
                             }

                         }
                         is  SaveViewModel.SaveNewsUiState.Error -> {
                             binding.tvFindSaveHere.visibility = View.INVISIBLE
                             Toast.makeText(context, getString(R.string.error) +it.exception.message, Toast.LENGTH_SHORT).show()
                         }
                         is  SaveViewModel.SaveNewsUiState.Loading -> {
                             binding.progressBar.visibility = View.VISIBLE
                             binding.tvFindSaveHere.visibility = View.INVISIBLE
                         }
                         is  SaveViewModel.SaveNewsUiState.NoSaves -> {
                             binding.recyclerNews.adapter = null
                             binding.tvFindSaveHere.visibility = View.VISIBLE
                             binding.progressBar.visibility = View.INVISIBLE
                         }
                     }
                 }
             }

        }


    }

    override fun onResume() {
        super.onResume()

        viewModel.loadNews()
    }



    override fun rowClicked(position: Int) {
        val intent = Intent(context, NewsDetailsActivity::class.java) //not application context
        intent.putExtra (NewsDetailsActivity.POSITION, position)
        intent.putExtra(NewsDetailsActivity.TYPE, NewsDetailsActivity.Save)
        startActivity(intent)
    }

    override fun rowClickedLong(number: Int) {



        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.confirm))
        builder.setMessage(getString(R.string.delete))
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        builder.setPositiveButton(getString(R.string.yes)){dialogInterface, which ->
            viewModel.deleteNews(number)
        }

        builder.setNeutralButton(getString(R.string.cancel)){dialogInterface , which ->

        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()


    }


}