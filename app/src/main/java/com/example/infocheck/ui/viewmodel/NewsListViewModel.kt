package com.example.infocheck.ui.viewmodel



import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.models.News
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class NewsListViewModel( private val repository : DatasRepository, private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : ViewModel()
//for testing purposes to override the dispatcher
{ @Inject constructor(repos : DatasRepository) : this(repos, Dispatchers.IO)



    private val _listNews: MutableStateFlow<NewsUiState> = MutableStateFlow(NewsUiState.Loading)
    val listNews: StateFlow<NewsUiState> = _listNews

    private var downloadJob : Job? = null

    init {
        //downloadNews(false)
    }



    fun downloadNews(forceDownload: Boolean) {

            _listNews.value = NewsUiState.Loading
            downloadJob =
                repository.downloadNews(downloadJob,forceDownload).onEach {

                    _listNews.value = it
                }
                    .catch { e-> _listNews.value = NewsUiState.Error(e) }
                    .flowOn(dispatcher)
                    .launchIn(viewModelScope)

    }




    sealed class NewsUiState {
        data class Success(val news:  List<News>): NewsUiState()
        object Loading : NewsUiState()
        object NoQuery : NewsUiState()
        data class Error(val exception: Throwable): NewsUiState()
    }



}