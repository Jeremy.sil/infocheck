package com.example.infocheck.ui.fragments


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.infocheck.R
import com.example.infocheck.databinding.NewsListFragmentBinding
import com.example.infocheck.ui.NewsDetailsActivity
import com.example.infocheck.ui.adapters.RecyclerInteractor
import com.example.infocheck.ui.adapters.RecyclerNewsAdapter
import com.example.infocheck.ui.viewmodel.NewsListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NewsListFragment: Fragment( ), RecyclerInteractor
{

    private var _binding: NewsListFragmentBinding? = null
    private val binding get() = _binding!!

    private val newsListViewModel : NewsListViewModel by viewModels()

    private  val recyclerInteractor = this



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.news_list_title)

        //Update list by pulling
        binding.swipeContainer.setOnRefreshListener {
            newsListViewModel.downloadNews(true)
            binding.swipeContainer.isRefreshing = false;
            binding.recyclerNews.adapter =  null

        }

        //listen to viewmodel listnews state flow changes to show the list of news in recycler view
        newsListViewModel.listNews.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach {
                when (it) {
                    is NewsListViewModel.NewsUiState.Success -> {
                        binding.tvAddsomequery.visibility = View.INVISIBLE
                        binding.progressBar.visibility = View.INVISIBLE

                        if (binding.recyclerNews.adapter == null) {

                            binding.recyclerNews.adapter = RecyclerNewsAdapter(it.news, recyclerInteractor)
                        }
                        (binding.recyclerNews.adapter as RecyclerNewsAdapter).setList(it.news)
                        binding.recyclerNews.adapter?.notifyDataSetChanged()


                    }
                    is NewsListViewModel.NewsUiState.Error -> {
                        binding.tvAddsomequery.visibility = View.INVISIBLE
                        binding.progressBar.visibility = View.INVISIBLE
                        Toast.makeText(
                            context,
                            getString(R.string.error) + it.exception.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    is NewsListViewModel.NewsUiState.Loading -> {
                        binding.tvAddsomequery.visibility = View.INVISIBLE
                        binding.progressBar.visibility = View.VISIBLE
                    }
                    is NewsListViewModel.NewsUiState.NoQuery -> {
                        binding.progressBar.visibility = View.INVISIBLE
                        binding.tvAddsomequery.visibility = View.VISIBLE
                    }

                }

            }
            .launchIn(lifecycleScope)



    }



    override fun onResume() {
        super.onResume()

        newsListViewModel.downloadNews(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = NewsListFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }


    override fun rowClicked(position: Int ){
        val intent = Intent(context, NewsDetailsActivity::class.java) //not application context
        intent.putExtra (NewsDetailsActivity.POSITION, position)
        intent.putExtra(NewsDetailsActivity.TYPE, NewsDetailsActivity.News)
        startActivity(intent)
    }

    override fun rowClickedLong(number: Int) {

    }


}
