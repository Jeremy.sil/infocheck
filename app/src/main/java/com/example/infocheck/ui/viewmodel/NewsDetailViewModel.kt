package com.example.infocheck.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.infocheck.datas.DatasRepository
import com.example.infocheck.datas.models.News
import com.example.infocheck.ui.NewsDetailsActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class NewsDetailViewModel( private val repository : DatasRepository, private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : ViewModel()
//for testing purposes to override the dispatcher
{ @Inject constructor(repos : DatasRepository) : this(repos, Dispatchers.IO)


    private val _news = MutableLiveData<News>()
    val news:  LiveData<News> get () =  _news

    private val _saveState: MutableStateFlow<SaveState> = MutableStateFlow(SaveState.Empty)
    val saveState: StateFlow<SaveState> = _saveState



    fun loadNews(position: Int, type : Int){
        if (position == -1){
            _news.postValue( News().apply { title = "Erreur de l'application" })

        }else{
            if(type == NewsDetailsActivity.News){
                _news.postValue( repository.getNews(position) )
            }else{

                _news.postValue( repository.getSavedNews(position) )
            }

        }

    }

    fun saveNews(position: Int) {

        repository.saveNews((position)).onEach {

            _saveState.value = it
        }
            .catch { e->  _saveState.value = SaveState.Error(e) }
            .flowOn(dispatcher)
            .launchIn(viewModelScope)
    }

    fun emptySaveState() {
        _saveState.value = SaveState.Empty

    }


    sealed class SaveState {
        object Success : SaveState()
        object Empty : SaveState()
        object AlreadySaved : SaveState()
        data class Error(val exception: Throwable): SaveState()
    }

}