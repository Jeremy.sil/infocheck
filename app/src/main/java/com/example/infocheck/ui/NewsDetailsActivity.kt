package com.example.infocheck.ui
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.infocheck.R
import com.example.infocheck.databinding.NewsDetailsActivityBinding
import com.example.infocheck.ui.viewmodel.NewsDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


@AndroidEntryPoint
class NewsDetailsActivity : AppCompatActivity() {


    private lateinit var  binding: NewsDetailsActivityBinding


    //The position is the id for the news in the repository,
    //news type means you opened the details from newslistActivity
    //save type means you opened details from saves news
    companion object {
        const val POSITION = "POSITION"
        const val TYPE = "TYPE"

        const val News  : Int = 1
        const val Save  : Int = 2

    }


    private val viewModel: NewsDetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = DataBindingUtil.inflate(layoutInflater, R.layout.news_details_activity, null, false)
        setContentView(binding.root)

        binding.newsViewModel = viewModel
        binding.lifecycleOwner = this

        var position : Int = -1
        var type = News


         supportActionBar?.title = getString(R.string.news_details_title)

        intent.extras?.let {
           position = it.getInt(POSITION)
            type = it.getInt(TYPE)
        }

        binding.position = position
        viewModel.loadNews(position,type)

        //the others fields use databinding
        viewModel.news.observe(this){
           binding.imageNewsDetails.setImageBitmap(it.imageNews)
        }


//Handle save state when clicking on button save
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.saveState.collect {

                    when (it) {
                        is NewsDetailViewModel.SaveState.Success -> {

                            Toast.makeText(
                                applicationContext,
                                getString(R.string.news_saved),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        is NewsDetailViewModel.SaveState.Error -> {
                            Toast.makeText(
                                applicationContext,
                                getString(R.string.error) + it.exception.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        is NewsDetailViewModel.SaveState.AlreadySaved -> {
                            Toast.makeText(
                                applicationContext,
                                getString(R.string.news_already_saved),
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }
                    viewModel.emptySaveState()
                }
            }
        }



        when (type){

           Save -> {
              binding.buttonSave.visibility = View.GONE
           }

        }


       binding.buttonKnowMore.setOnClickListener {

           viewModel.news.value?.url?.let {
               val intent = Intent(Intent.ACTION_VIEW)
               intent.data = Uri.parse(it)
               startActivity(intent)
           }
       }
/*
        binding.buttonSave.setOnClickListener{
             viewModel.saveNews(position)

        }*/
    }





}
