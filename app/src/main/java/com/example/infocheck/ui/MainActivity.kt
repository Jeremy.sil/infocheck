package com.example.infocheck.ui


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.example.infocheck.R
import com.example.infocheck.databinding.MainActivityBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint


// MainActivity that contains the 3 fragments configfragment, newslistFragment and SaveFragment
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding



    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.newsListFragment -> {

                return@OnNavigationItemSelectedListener true
            }
            R.id.configFragment -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.saveFragment -> {
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        setUpNavigation()

    }

    fun setUpNavigation() {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment?
        NavigationUI.setupWithNavController(
           binding.navView,
            navHostFragment!!.navController
        )

    }

}
